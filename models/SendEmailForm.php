<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

class SendEmailForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email','required'],
            ['email', 'email'],
            [
                'email', 'exist', 'targetClass' => User::className(),
                'filter' => [
                    'status' => User::STATUS_ACTIVE
                ],
                'message' => 'Данный email не зарегистрирован.'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
          'email' => 'Email'
        ];
    }

    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne(
            [
                'status' => User::STATUS_ACTIVE,
                'email' => $this->email
            ]
        );
        if ($user) {

            $user->generateSecretKey();

            if ($user->save()) {
                try {
                    $result = Yii::$app->mailer->compose('resetPassword', ['user' => $user])
                        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name.' (отправлено роботом)'])
                        ->setTo($this->email)
                        ->setSubject('Активация акаунта для '.Yii::$app->name)
                        ->send();
                } catch (Exception $e) {
                    $result = false;
                }

                return $result;
            }
        }

        return false;
    }

}