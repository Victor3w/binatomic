<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadFile extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $image;

    public function rules()
    {
        return [
            [['image'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'png, jpg',
                'maxFiles' => 5,
                'maxSize' => 10485760
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => 'Фотографии'
        ];
    }
    public function upload($id)
    {
        $images = [];

        if ($this->validate()) {

            if ($this->image) {

                $this->setDirectory($id);

                foreach ($this->image as $file) {
                    $file->saveAs(Yii::getAlias('@webroot/files/profiles/') . $id . '/'. $file->baseName . '.' . $file->extension);
                    $this->setChmod($id . '/'. $file->baseName . '.' . $file->extension);
                    $images[] = '/files/profiles/' . $id . '/'. $file->baseName . '.' . $file->extension;
                }
                return $images;
            }else {

                return $images;
            }
        } else {
            return $images;
        }
    }

    public function setDirectory($pass)
    {
        if (!file_exists(Yii::getAlias('@webroot/files/profiles/') . $pass)) {
            mkdir(Yii::getAlias('@webroot/files/profiles/') . $pass, 0777, true);
            chmod(Yii::getAlias('@webroot/files/profiles/') . $pass, 0777);
        }
        return $pass;
    }
    public function setChmod($pass)
    {
        if (file_exists(Yii::getAlias('@webroot/files/profiles/') . $pass)) {
            chmod(Yii::getAlias('@webroot/files/profiles/') . $pass, 0777);
        }

    }
}