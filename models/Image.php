<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property integer $status
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $secret_key
 *
 * @property Profile $profile
 */
class Image extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image', 'string','max'=>'255'],
            ['profile_id', 'integer']
        ];
    }
    /* Поведения */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function addImages($images, $profile_id)
    {
        if (empty($images)) {
            return null;
        }

        foreach ($images as $file) {
            $image = new Image();
            $image->image = $file;
            $image->profile_id = $profile_id;
            $image->save();
        }

    }
    public function deleteImage($id){

        try {
            $image = Image::findOne($id);
            $profile_id = $image->profile_id;
            $main_image = $image->image;
            $image->delete();

            unlink(Yii::getAlias('@webroot' . $main_image));
            unlink(Yii::getAlias('@webroot/resize/' . $main_image));
            return $profile_id;
        } catch (\Exception  $e) {
            return null;
        }
    }
    public function addImage($id)
    {
        $images = Image::findAll(['profile_id' => $id]);

        if (count($images) >= $images) {
            return null;
        }



    }
}