<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\Image as ModelImage;
use yii\imagine\Image;
use yii\helpers\Html;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property integer $status
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $secret_key
 *
 * @property Profile $profile
 */
class Profile extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profiles';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'address', 'phone'], 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['name'], 'required'],
            ['name','string','min' => 2, 'max' => 100],
            ['address', 'string', 'max' => 150],
            ['phone', 'string', 'length' => 19],

        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'address' => 'Адрес',
            'phone' => 'Номер телефона'
        ];
    }
    /* Связи */
    public function getImages()
    {
        return $this->hasMany(ModelImage::className(), ['profile_id' => 'id']);
    }
    public function deleteProfile($id)
    {
        try {
            $profile = Profile::findOne($id);
            $profile->delete();

            ModelImage::deleteAll(['profile_id' => $id]);

            rmdir(Yii::getAlias('@webroot/files/profiles/' . $id));
            rmdir(Yii::getAlias('@webroot/resize/files/profiles/' . $id));
            return true;
        } catch (\Exception  $e) {
            return false;

        }
    }
    /* Поведения */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function addProfile()
    {

        if (!$this->validate()) {
            return null;
        }

        $profile = new Profile();
        $profile->name = $this->name;
        $profile->email = $this->email;
        $profile->address = $this->address;
        $profile->phone = $this->phone;
        $profile->user_id = Yii::$app->user->identity->id;

        return $profile->save() ? $profile : null;
    }

    public function updateProfile($profile_id)
    {

        if (!$this->validate()) {
            return null;
        }

        $profile = Profile::findOne($profile_id);
        $profile->name = $this->name;
        $profile->email = $this->email;
        $profile->address = $this->address;
        $profile->phone = $this->phone;
        $profile->user_id = Yii::$app->user->identity->id;

        return $profile->save() ? $profile : null;
    }
    public function getSlides()
    {
        $images = [];

        if ($this->getImages()->count() > 0) {

            foreach ($this->getImages()->all() as $image) {
                $images[] = "<a href='$image->image' target='_blank'><img src='". $this->uploadThumbnail($image->profile_id, $image->image) . "'></a>";
            }

            return $images;

        }
        return $images;
    }
    public function uploadThumbnail($id, $image)
    {
        if (!file_exists(Yii::getAlias('@webroot/resize') . $image)) {

            $this->setDirectory((int)$id);

            Image::thumbnail(Yii::getAlias('@webroot') . $image, 100, 100)
                ->save(Yii::getAlias('@webroot/resize') . $image,['quality' => 70]);

            $this->setChmod($image);
        }

        return '/resize' . $image;
    }

    public function setDirectory($pass)
    {
        if (!file_exists(Yii::getAlias('@webroot/resize/files/profiles/') . $pass)) {
            mkdir(Yii::getAlias('@webroot/resize/files/profiles/') . $pass, 0777, true);
            chmod(Yii::getAlias('@webroot/resize/files/profiles/') . $pass, 0777);
        }
    }
    public function setChmod($pass)
    {
        if (file_exists(Yii::getAlias('@webroot/resize') . $pass)) {
            chmod(Yii::getAlias('@webroot/resize') . $pass, 0777);
        }

    }
}