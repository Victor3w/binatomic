<?php

return [
    'user.passwordResetTokenExpire' => 3600,
    'host_cache' => "127.0.0.1",
    'port_cache' => 11211,
    'supportEmail' => 'binatomic3w@gmail.com',
    'secretKeyExpire' => 60 * 60,                       // время хранения секретного ключа
    'emailActivation' => true,                         // активация по емайл
    'loginWithEmail' => true,
];
