<?php use kartik\file\FileInput; ?>
<?= FileInput::widget([
    'model' => $image,
    'attribute' => 'image[]',
    'options' => ['multiple' => true, 'accept' => 'image/*'],
    'disabled' => !$countFile ? true : false,
    'pluginOptions' => [
        'allowedFileExtensions'=>['jpg', 'png'],
        'showPreview' => true,
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => false,
        'maxFileCount' => $countFile,
        'mainClass' => 'disabled'
    ]
]);?>