
<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

$menuItems = [
    ['label' => 'Домашняя страница', 'url' => ['/']],
];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Регистрация', 'url' => ['/signup']];
    $menuItems[] = ['label' => 'Войти', 'url' => ['/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/logout'], 'post')
        . Html::submitButton(
            'Выйти (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
    $menuItems[] = ['label' => 'Профиль', 'url' => ['/profile']];
    $menuItems[] = ['label' => 'Мои настройки', 'url' => ['/settings']];

}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);

NavBar::end();
?>