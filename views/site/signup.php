<?php use yii\helpers\Html; ?>
<?php use yii\bootstrap\ActiveForm; ?>
<?php $this->title = 'Регистрация'; ?>
<?php $this->params['breadcrumbs'][] = $this->title; ?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Чтобы использовать возможности этого приложения, зарегистрируйтесь</p>
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if ($model->scenario === 'emailActivation'): ?>
                <i>На указанный email придет письмо для активации аккаунта</i>
            <?php endif; ?>

        </div>
    </div>
</div>