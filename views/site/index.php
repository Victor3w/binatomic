<?php

/* @var $this yii\web\View */

$this->title = $title;
?>
<div class="site-index">

    <?php if(count($profiles) > 0): ?>
        <div class="m-t-30">
            <table class="table table-striped table-bordered data-table">
                <thead>
                <tr>
                    <th width="30">ID</th>
                    <th width="100" class="text-center">Имя</th>
                    <th width="100" class="text-center">Email</th>
                    <th width="150" class="text-center">Адрес</th>
                    <th width="20" class="text-center">Номер телефона</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($profiles as $count => $profile): ?>
                    <tr>
                        <td>
                            <?= ++$count; ?>
                        </td>
                        <td class="text-right">
                            <?= $profile->name ?>
                        </td>
                        <td class="text-right">
                            <?= $profile->email ?>
                        </td>
                        <td class="text-center">
                            <?= $profile->address ?>
                        </td>
                        <td class="text-center">
                            <a href="tel:<?= $profile->phone; ?> ">
                                <?= $profile->phone; ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

    <?php endif; ?>
</div>
