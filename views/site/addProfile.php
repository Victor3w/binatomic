<?php use yii\helpers\Html; ?>
<?php use yii\widgets\ActiveForm; ?>
<?php use yii\widgets\MaskedInput; ?>
<?php use kartik\file\FileInput; ?>

<?php
$this->render('@app/views/includes/seo', [
    'title' => $title ?? Yii::$app->name,
    'keywords' => $keywords ?? '',
    'description' => $description ?? ''
]); ?>

<?php $this->params['breadcrumbs'][] = $link_profile; ?>
<?php $this->params['breadcrumbs'][] = $title; ?>
<div class="site-about">
    <h1><?= Html::encode($title) ?></h1>


    <div class="hidden">
        <div class="alert">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <span>

            </span>

        </div>
    </div>


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data',  'class' => 'add_profile',]]) ?>
    <?= $form->field($profile, 'name')->textInput() ?>
    <?=  $form->field($profile, 'email')->input('email') ?>
    <?=  $form->field($profile, 'address')->textInput() ?>
    <?=  $form->field($profile, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+375 (99) 999-99-99',
        'options' => [
            'class' => 'form-control placeholder-style',
            'placeholder' => ('Телефон')
        ],
        'clientOptions' => [
            'clearIncomplete' => true
        ]
    ]) ?>

    <?= $form->field($image, 'image[]')->widget(FileInput::classname(), [
        'name' => 'test[]',
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions'=>['jpg', 'png'],
            'showPreview' => true,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-add-profile']);?>

    <?php ActiveForm::end() ?>
</div>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;load=SuggestView&amp;onload=onLoad"></script>
<script>
    function onLoad (ymaps) {
        var suggestView = new ymaps.SuggestView('profile-address', {results: 5, offset: [20, 30]});
    }
</script>