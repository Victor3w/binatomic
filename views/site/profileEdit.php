<?php use yii\helpers\Html; ?>
<?php use yii\widgets\ActiveForm; ?>
<?php use yii\widgets\MaskedInput; ?>
<?php use kartik\file\FileInput; ?>

<?php
$this->render('@app/views/includes/seo', [
    'title' => $title ?? Yii::$app->name,
    'keywords' => $keywords ?? '',
    'description' => $description ?? ''
]); ?>

<?php $this->params['breadcrumbs'][] = $link_profile; ?>
<?php $this->params['breadcrumbs'][] = $title; ?>
<div class="site-about">
    <h1><?= Html::encode($title) ?></h1>


    <div class="hidden">
        <div class="alert">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <span>

            </span>

        </div>
    </div>


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data',  'class' => 'add_profile',]]) ?>
    <?= $form->field($profile, 'name')->textInput() ?>
    <input type="hidden" name="id" value="<?= $id ?>">

    <?=  $form->field($profile, 'email')->input('email') ?>
    <?=  $form->field($profile, 'address')->textInput() ?>
    <?=  $form->field($profile, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+375 (99) 999-99-99',
        'options' => [
            'class' => 'form-control placeholder-style',
            'placeholder' => ('Телефон')
        ],
        'clientOptions' => [
            'clearIncomplete' => true
        ]
    ]) ?>
    <div class="file-image-change">
        <?php include(Yii::getAlias('@app/views/includes/file_image.php')); ?>
    </div>

    <?php if(count($images) > 0): ?>
        <div class="m-t-30">
            <table class="table table-striped table-bordered data-table">
                <thead>
                <tr>
                    <th width="10" class="text-center">ID</th>
                    <th width="100" class="text-center">Фото</th>
                    <th width="10" class="text-center">
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($images as $count => $image): ?>
                    <tr>
                        <td>
                            <?= ++$count; ?>
                        </td>
                        <td class="text-center">
                            <a href="<?= $image->image?>" target="_blank">
                                <img src="<?= $profile->uploadThumbnail($id, $image->image)?>" alt="">
                            </a>
                        </td>
                        <td  class="text-center">
                            <a href="<?= \yii\helpers\Url::to(['/image-delete', 'id' => $image->id]); ?>"
                               class="link-image-del" data-id="<?= $image->id?>">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

    <?php endif; ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-add-profile']);?>

    <?php ActiveForm::end() ?>
</div>

<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;load=SuggestView&amp;onload=onLoad"></script>
<script>
    function onLoad (ymaps) {
        var suggestView = new ymaps.SuggestView('profile-address', {results: 5, offset: [20, 30]});
    }
</script>