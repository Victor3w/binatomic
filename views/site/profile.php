<?php use yii\helpers\Html; ?>
<?php use yii\helpers\Url; ?>

<?php
$this->render('@app/views/includes/seo', [
    'title' => $title ?? Yii::$app->name,
    'keywords' => $keywords ?? '',
    'description' => $description ?? ''
]); ?>

<?php $this->params['breadcrumbs'][] = $title; ?>
<div class="site-about">
    <h1><?= Html::encode($title) ?></h1>


    <div class="hidden">
        <div class="alert">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <span>

            </span>

        </div>
    </div>
    <p>
        <a href="/add-profile">Добавить новый профиль</a>
    </p>
    <?php if(count($users) > 0): ?>
        <div class="m-t-30">
            <table class="table table-striped table-bordered data-table">
                <thead>
                <tr>
                    <th width="30">ID</th>
                    <th width="150" class="text-center">Имя</th>
                    <th width="150" class="text-center">Email</th>
                    <th width="100" class="text-center">Фото</th>
                    <th width="10" class="text-center"></th>
                    <th width="10" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($users as $count => $user): ?>
                    <tr>
                        <td>
                            <?= ++$count; ?>
                        </td>
                        <td class="text-right">
                            <?= $user->name?>
                        </td>
                        <td class="text-right">
                            <?= $user->email?>
                        </td>
                        <td class="text-center">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php foreach($user->getSlides() as $image): ?>
                                        <div class="swiper-slide">
                                            <?= $image?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </td>
                        <td class="text-center">
                            <a href="<?= Url::to(['/profile-edit/'.$user->id]); ?>"
                               class="link-edit" data-id="<?= $user->id?>">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </td>
                        <td  class="text-center">
                            <a href="<?= Url::to(['/profile-delete', 'id' => $user->id]); ?>"
                               class="link-del" data-id="<?= $user->id?>">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

    <?php endif; ?>

</div>
