<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class AddController extends Controller
{
    public function getDataCache($data, $key, $duration) {

        if (!$data || !$key || !$duration) {
            return '';
        }

        $memcache = new \Memcached;
        $memcache->setOption(\Memcached::OPT_COMPRESSION, false);
        $memcache->addServers([
                [
                    "HOST" => Yii::$app->params["host_cache"],
                    "PORT" => Yii::$app->params["port_cache"]
                ]
            ]);

        if ($memcache->get($key) === false) {

            $memcache->set($key,$data,$duration);
        }

        return $memcache->get($key);
    }
}