<?php

namespace app\controllers;

use app\models\Image;
use app\models\Profile;
use app\models\SendEmailForm;
use app\models\UploadFile;
use app\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AccountActivation;
use app\models\ResetPasswordForm;
use yii\web\UploadedFile;

class SiteController extends AddController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'logout',
                            'reset-password',
                            'activate-account',
                            'add-profile',
                            'profile-delete',
                            'profile-edit',
                            'image-delete',
                            'add-image'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index',[

            'title' => Yii::$app->name,
            'keywords' => '',
            'description' => '',
            'profiles' => Profile::find()->all()
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('profile');
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays profile page.
     *
     * @return string
     */
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('profile', [
            'title' => Yii::$app->user->identity->username ?? '',
            'keywords' => '',
            'description' => '',
            'users' => Profile::find()->where(['user_id' => Yii::$app->user->identity->id])->all()
        ]);
    }

    public function actionAddProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $image = new UploadFile();
        $profile = new Profile();

//
//        if (Yii::$app->request->isAjax) {
//
//            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($profile->load(Yii::$app->request->post()) && $image->load(Yii::$app->request->post())) {


                if ($new_profile = $profile->addProfile()) {
                    $image->image = UploadedFile::getInstances($image, 'image');

                    if ($images = $image->upload($new_profile->id)) {
                        $save_image = new Image();
                        $save_image->addImages($images, $new_profile->id);

                    }
                    return $this->redirect('/profile');

                } else {

                    return $this->redirect('/add-profile');

                }
            }
//        }

        return $this->render('addProfile', [
            'title' => 'Создание нового профиля',
            'keywords' => '',
            'description' => '',
            'profile' => $profile,
            'id' => null,
            'image' => $image,
            'link_profile' => ['label' => Yii::$app->user->identity->username, 'url' => '/profile'],


        ]);

    }

    /**
     * Displays setting page.
     *
     * @return string
     */
    public function actionProfileDelete($id=0)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $profile = new Profile();

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post()) {

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $id = (int)Yii::$app->request->post()['id'];
                $profile->deleteProfile($id);
                return $profile;
            }
        }

        $profile->deleteProfile((int)$id);

        return $this->redirect('/profile');

    }
    /**
     * Displays setting page.
     *
     * @return string
     */
    public function actionImageDelete($id=0)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Image();

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->post()) {

                $id = (int)Yii::$app->request->post()['id'];


                $profile = $model->deleteImage($id);

                return $this->redirect('/profile-edit/' . $profile);
            }
        }

        return $this->redirect('/profile-edit/' . $model->deleteImage((int)$id));

    }
    /**
     * Displays setting page.
     *
     * @return string
     */
    public function actionProfileEdit($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $profile = Profile::findOne((int)$id);
        $images = Image::findAll(['profile_id' => (int)$id]);

        $image = new UploadFile();

        if ($profile->load(Yii::$app->request->post()) && $image->load(Yii::$app->request->post())) {

            $profile_id = (int)Yii::$app->request->post()['id'];

            if ($profile->updateProfile($profile_id)) {
                $image->image = UploadedFile::getInstances($image, 'image');

                if ($images = $image->upload($profile_id)) {
                    $save_image = new Image();
                    $save_image->addImages($images, $profile_id);

                }
                return $this->redirect('/profile-edit/'.$profile_id);

            } else {

                return $this->redirect('/profile-edit/'.$profile_id);

            }
        }

        return $this->render('profileEdit', [
            'title' => 'Редактирование профиля',
            'id' => $id,
            'keywords' => '',
            'description' => '',
            'profile' => $profile,
            'image' => $image,
            'countFile' => (count($images) >= 5) ? null : 5 - count($images),
            'images' => $images,
            'link_profile' => ['label' => Yii::$app->user->identity->username, 'url' => '/profile'],


        ]);

    }


    public function actionSignup()
    {
        $model = Yii::$app->params['emailActivation']
            ? new SignupForm(['scenario' => 'emailActivation'])
            : new SignupForm();


        if ($model->load(Yii::$app->request->post())) {

            if ($user = $model->signup()) {

                if ($user->status === User::STATUS_ACTIVE) {

                    if (Yii::$app->getUser()->login($user)) {
                        return $this->redirect('profile');
                    }
                } else {

                    if ($model->sendActivationEmail($user)) {
                        Yii::$app->session->setFlash(
                            'success',
                            'Письмо отправлено по email <strong>'
                                . Html::encode($user->email)
                                . '</strong> (проверьте папку спам)');
                    } else {
                        Yii::$app->session->setFlash(
                            'error',
                            'Ошибка, письмо не отправлено.'
                        );

                        Yii::error('Ошибка отправки письма');
                    }

                    return $this->refresh();
                }
            } else {
                Yii::$app->session->setFlash(
                    'error',
                    'Возникла ошибка при регистрации.'
                );

                Yii::error('Ошибка отправки письма');

                return $this->refresh();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }


    public function actionSendEmail()
    {
        $model = new SendEmailForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                if ($model->sendEmail()) {

                    Yii::$app->getSession()->setFlash(
                        'warning',
                        'Проверьте емайл.');

                    return $this->redirect('profile');
                } else {

                    Yii::$app->getSession()->setFlash(
                        'error',
                        'Нельзя сбросить пароль.');
                }
            }
        }
        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($key)
    {
        try {
            $model = new ResetPasswordForm($key);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate() && $model->resetPassword()) {

                Yii::$app->getSession()->setFlash('warning', 'Пароль изменен.');

                return $this->redirect(['/login']);
            }
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionActivateAccount($key)
    {
        try {
            $user = new AccountActivation($key);
        } catch(InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($user->activateAccount()) {

            Yii::$app->session->setFlash(
                'success',
                'Активация прошла успешно. <strong>'
                    . Html::encode($user->username)
                    . '</strong>');
        } else {

            Yii::$app->session->setFlash('error', 'Ошибка активации.');
            Yii::error('Ошибка при активации.');
        }

        return $this->redirect(Url::to(['/login']));
    }
}
