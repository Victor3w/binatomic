$( document ).ready(function() {
    if ($('.swiper-container').length) {
        var swiper = new Swiper('.swiper-container');
    }

    $('.link-del').on('click', function(e){
        e.preventDefault();
        
        var id = $('.link-del').data('id');
        $.ajax({
            type:'POST',
            cache: false,
            data: {id: id},
            url: '/profile-delete',
            success  : function(response) {
                if (response) {

                    $('[data-id="'+id+'"]').closest('tr').remove();
                }
            }
        });
        
    });
    $('.link-image-del').on('click', function(e){
        e.preventDefault();

        var id = $('.link-image-del').data('id');
        $.ajax({
            type:'POST',
            cache: false,
            data: {id: id},
            url: '/image-delete',
            success  : function(response) {
                if (response.success) {
                }
            }
        });

    });

    var tableResponsive = false;
    if ($(window).width() < 768) {
        tableResponsive = true;
    }


    if($('.data-table').length){
        $('.data-table').dataTable({
            responsive: tableResponsive,
            processing: true,
            aaSorting: [],
            columnDefs: [{
                "targets": 'no-sort',
                "orderable": false
            }],
            sPaginationType: "full_numbers",
            oLanguage: {
                sProcessing: "Подождите...",
                sSearch: "Поиск:",
                sLengthMenu: "Показать _MENU_",
                sInfo: "",
                sInfoEmpty: "",
                sInfoFiltered: "",
                sInfoPostFix: "",
                sLoadingRecords: "Загрузка записей...",
                sZeroRecords: "Записи отсутствуют.",
                sEmptyTable: "Данные отсутствуют",
                oPaginate: {
                    sFirst: "",
                    sPrevious: "Предыдущая",
                    sNext: "Следующая",
                    sLast: ""
                },
                oAria: {
                    sSortAscending: ": активировать для сортировки столбца по возрастанию",
                    sSortDescending: ": активировать для сортировки столбца по убыванию"
                },
                sSearchPlaceholder: "Поиск..."
            },
            pageLength: 10
        });
    }
});